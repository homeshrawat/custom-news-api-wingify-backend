package com.newsapi.controller;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.newsapi.customResponseDTO.CustomArticle;
import com.newsapi.customResponseDTO.CustomResponse;
import com.newsapi.newsResponseDTO.NewsApiResponse;
import com.newsapi.newsResponseDTO.NewsArticle;
import com.newsapi.util.*;

@RestController
public class CustomRestApiController {
	
	ObjectMapper mapper = new ObjectMapper();

	@RequestMapping("/articles")
	public ResponseEntity<?> headLines(@RequestParam(value = "country") String country,
			@RequestParam(value = "category") String category, @RequestParam(value = "keyword") String keyword)
			throws UnsupportedEncodingException, MissingServletRequestParameterException {
		Map<String, String> params = new HashMap<>();
		params.put("country", country);
		params.put("category", category);

		String paramString = Util.createURL(params);
		String uri;
		if (paramString.length() != 0) {
			uri = Constants.api + "&" + paramString;
		} else {
			uri = Constants.api;
		}
		RestTemplate restTemplate = new RestTemplate();
		NewsApiResponse response = restTemplate.getForObject(uri, NewsApiResponse.class);

		List<CustomArticle> responseList = new ArrayList<>();
		try {
			for (NewsArticle article : response.getArticles()) {
				if (Util.checkString(article.toString(), keyword)) {
					responseList.add(new CustomArticle(article.getTitle(), article.getDescription(), article.getUrl()));
				}
			}
		} catch (Exception e) {

		}
		CustomResponse result = new CustomResponse();
		result.setStatus("OK");
		result.setArticles(responseList);
		result.setCategory(category);
		result.setCountry(country);
		result.setKeyword(keyword);

		return new ResponseEntity<CustomResponse>(result, HttpStatus.OK);
	}

}
