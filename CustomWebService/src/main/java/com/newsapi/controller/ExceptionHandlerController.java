package com.newsapi.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.newsapi.customResponseDTO.ErrorResponse;


@ControllerAdvice
public class ExceptionHandlerController {

	@ExceptionHandler(MissingServletRequestParameterException.class)
	@ResponseBody
	public ErrorResponse handleMissingParam(MissingServletRequestParameterException ex) {
		return new ErrorResponse(HttpStatus.BAD_REQUEST.toString(), "parameter is missing - " + ex.getParameterName());
	}

	@ExceptionHandler(NoHandlerFoundException.class)
	@ResponseBody
	public ErrorResponse requestHandlingNoHandlerFound() {
		return new ErrorResponse(HttpStatus.BAD_REQUEST.toString(), "resource not found");
	}

}
