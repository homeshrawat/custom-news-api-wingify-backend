package com.newsapi.customResponseDTO;

import java.util.List;


public class CustomResponse {

	private String status;
	private String country;
	private String category;
	private String keyword;
	private List<CustomArticle> articles;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public List<CustomArticle> getArticles() {
		return articles;
	}

	public void setArticles(List<CustomArticle> articles) {
		this.articles = articles;
	}

}
