package com.newsapi.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;
import java.util.Map.Entry;

public class Util {
	public static String createURL(Map<String, String> params) throws UnsupportedEncodingException {
		StringBuilder uri = new StringBuilder();

		for (Entry<String, String> entry : params.entrySet()) {
			uri.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
			uri.append("=");
			uri.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
			uri.append("&");
		}

		return uri.length() > 0 ? uri.substring(0, uri.length() - 1).toString() : uri.toString();
	}
	
	public static boolean checkString(String str, String keyword) {
		String[] words = str.split(" ");
		for (String word : words) {
			if (word.equalsIgnoreCase(keyword)) {
				return true;
			}
		}
		return false;
	}
}
